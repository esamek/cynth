function objectString(object) {
    var output = '';
    for (var property in object) {
        output += property + ': ' + object[property] + '; ';
    }
    return output;
}

function getGesture(event) {
    return event.gesture;
}


function debugGesture(e) {
    var gesture = e.gesture;
    var props = {};

    props.angle = gesture.angle;
    props.centerX = gesture.center.pageX;
    props.centerY = gesture.center.pageY;
    props.deltaTime = gesture.deltaTime;
    props.deltaX = gesture.deltaX;
    props.deltaY = gesture.deltaY;
    props.direction = gesture.direction;
    props.distance = gesture.distance;
    props.eventType = gesture.eventType;
    props.interimAngle = gesture.interimAngle;
    props.interimDirection = gesture.interimDirection;
    props.pointerType = gesture.pointerType;
    props.rotation = gesture.rotation;
    props.scale = gesture.scale;
    // props.srcEvent = gesture.srcEvent;
    // props.startEvent = gesture.startEvent;
    // props.target = gesture.target;
    props.timeStamp = gesture.timeStamp;
    // props.touches = gesture.touches;
    props.velocityX = gesture.velocityX;
    props.velocityY = gesture.velocityY;


    var html = '';

    for (var key in props) {
        html += '<strong>' + key + '</strong>: ' + props[key] + '<br />';
    }




    return html;


}





function init() {

    // settings

    var OCTAVES = 1;


    // notes, midi, freq, keycodes, timbre


    var dict = T("ndict.key");
    var midicps = T("midicps");

    var keycodes = [
        90, 83, 88, 68, 67, 86, 71, 66, 72, 78, 74, 77, 81,
        50, 87, 51, 69, 82, 53, 84, 54, 89, 55, 85, 73, 57, 79, 48, 80
    ];
    var notes = [
        //'C2', 'C#2', 'D2', 'D#2', 'E2', 'F2', 'F#2', 'G2', 'G#2', 'A2', 'A#2', 'B2',
        'C3', 'C#3', 'D3', 'D#3', 'E3', 'F3', 'F#3', 'G3', 'G#3', 'A3', 'A#3', 'B3',
        'C4', 'C#4', 'D4', 'D#4', 'E4', 'F4', 'F#4', 'G4', 'G#4', 'A4', 'A#4', 'B4',
        'C5', 'C#5', 'D5', 'D#5', 'E5',
    ];


    keycodes.valid = function(num) {
        return keycodes.indexOf(num) !== -1;
    }

    keycodes.getMIDI = function(keycode) {
        return dict.at(keycode);
    };

    keycodes.getFreq = function(midi) {
        if (midi) {

            var midi = keycodes.getMIDI(midi);
            return midicps.at(midi);
        } else {
            return false;
        }

    };


    var noteData = keycodes.map(function(d, i) {
        var midi = keycodes.getMIDI(d);
        var freq = keycodes.getFreq(midi);
        return {
            'keyCode': d,
            'note': notes[i],
            'midi': midi,
            'freq': freq
        }
    });



    // SYNTH
    var SYNTH = T("OscGen", {
        wave: "saw",
        mul: 1
    });


    if (navigator.userAgent.indexOf('iPad') != -1) {

        $('div').on('click', function() {
            SYNTH.play();
        });

    } else {
        SYNTH.play()
    }


    function noteOn(keycode) {
        if (keycodes.valid(keycode)) {
            var freq = keycodes.getFreq(keycode);
            if (keycode) SYNTH.noteOnWithFreq(freq, 100);
        }

    }

    function noteOff(keycode) {
        if (keycodes.valid(keycode)) {
            var freq = keycodes.getFreq(keycode);
            if (keycode) SYNTH.noteOnWithFreq(freq, 100);
        }
    }


    // dimensions


    function dimensions($w, $d) {
        return {
            w: $w.outerWidth(),
            h: $w.outerHeight(),
            keyCount: OCTAVES * 12, // 12 notes in an octave
            keyWidth: $w.outerWidth() / (OCTAVES * 12)

        };
    }





    // interface

    var $doc = $(document);
    var $win = $(window);
    var $body = $('body');
    var dims = dimensions($win, $doc);

    $win.css('width', dims.w);
    $win.css('height', dims.h);

    var scales = {};


    // pitch

    var pitchRange = noteData.map(function(d, i) {
        return d.freq;
    });

    scales.pitch = d3.scale.linear()
        .domain([0, dims.h])
        .range(notes);




    Hammer($body.get(0)).on('tap', function(e) {
        e.preventDefault();
        $('div').html('');
        $('div').html(scales.pitch(e.gesture.center.pageY));

        var velocity = e.gesture.velocityY;
        console.log(e.gesture.center.pageY, scales.pitch(e.gesture.center.pageY));

        SYNTH.noteOn(scales.pitch(e.gesture.center.pageY), 40);

    });






    // var data = noteData;

    // var WIDTH = dims.w;
    // var HEIGHT = dims.h;

    // var COLOR_BLACK = '#555';
    // var COLOR_BLUE = '#3498db';


    // var canvas = d3.select($body.get(0)).append('svg')
    //     .attr('width', WIDTH)
    //     .attr('height', HEIGHT);


    // var keys = canvas.selectAll('.key')
    //     .data(data).enter()
    //     .append('rect')
    //     .attr('class', 'key')
    //     .attr('width', dims.keyWidth)
    //     .attr('height', dims.h)
    //     .attr('fill', function(d, i) {
    //         var sharp = d.note.indexOf('#');
    //         return sharp !== -1 ? COLOR_BLACK : COLOR_BLUE;
    //     })
    //     .attr('x', function(d, i) {
    //         return i * dims.keyWidth;
    //     });







    // // events

    // canvas.selectAll('.key').each(function(d, i) {

    //     Hammer(this).on('touch', function(e) {

    //         noteOn(d.keyCode);
    //     });
    //     Hammer(this).on('release', function(e) {
    //         noteOff(d.keyCode);
    //     });

    // });


    // $doc.on('keydown', noteOn)
    //     .on('keyup', noteOff);



}; // end


// HAMMER EVENTS
// hold
// tap
// doubletap
// drag, dragstart, dragend, dragup, dragdown, dragleft, dragright
// swipe, swipeup, swipedown, swipeleft, swiperight
// transform, transformstart, transformend
// rotate
// pinch, pinchin, pinchout
// touch (gesture detection starts)
// release (gesture detection ends)

function ui() {

    var $win = $(window),
        $doc = $(document),
        $body = $('body'),
        svg = d3.select('body').append('svg');


    $.fn.Hammer = function(event, options, callback) {
        if (typeof options == 'function') {
            return Hammer(this.get(0)).on(event, options);
        } else {
            return Hammer(this.get(0)).on(event, options, callback);
        }
    };





    $body.css('width', '100%')
        .css('height', '100%')
        .css('overflow', 'hidden');

    svg.attr('width', '100%')
        .attr('height', '100%');


    function createCircle(cx, cy) {
        svg.append('circle')
            .attr('cx', cx)
            .attr('cy', cy)
            .attr('r', 20);
    }



    $body.Hammer('touch', function(e) {

        var g = getGesture(e);

        var cx = g.center.pageX,
            cy = g.center.pageY;

        createCircle(cx, cy);

    });

    $body.Hammer('release', function(e) {
        svg.selectAll('circle').remove();
    });

    $body.Hammer('drag', function(e) {
        var g = getGesture(e);
        var r = g.distance;
        svg.selectAll('circle')
            .transition()
            .attr('r', r)
            .delay(0)
            .duration(100)
            .ease('linear');

    });




} //function ui()


document.ontouchmove = function(event) {
    event.preventDefault();
}


document.addEventListener('DOMContentLoaded', function(e) {
    ui()
});